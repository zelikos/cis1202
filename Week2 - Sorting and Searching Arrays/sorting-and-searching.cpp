// Patrick Csikos
// CIS 1202 502
// 2020-09-04

#include <fstream>
#include <iomanip>
#include <iostream>

void loadArrays (std::string[], int[], int &);
void showArrays (std::string[], int[], int);
void lookUpPrice (std::string[], int[], int);
void sortPrices (std::string[], int[], int);
void highestPrice (std::string[], int[], int);
int showMenu (); // display a menu and return the user's choice

const int SIZE = 20;

int main () {
    std::string consoleNames[SIZE];
    int consolePrices[SIZE];
    int count = 0;

    loadArrays (consoleNames, consolePrices, count);

    int choice;
    do {
        choice = showMenu();
        switch (choice) {
            case 1:
                showArrays (consoleNames, consolePrices, count);
                break;
            case 2:
                lookUpPrice (consoleNames, consolePrices, count);
                break;
            case 3:
                sortPrices (consoleNames, consolePrices, count);
                break;
            case 4:
                highestPrice (consoleNames, consolePrices, count);
                break;
            case 5:
                std::cout << "Exiting...\n";
                break;
        }
    } while (choice != 5);

    return 0;
}

void loadArrays (std::string names[], int prices[], int& count) {
    std::ifstream consoleData;
    std::string temp;

    consoleData.open("02prices.txt");
    if (!consoleData) {
        std::cout << "File not found.\n";
    } else {
        std::cout << "Console data loaded successfully.\n";
        while (std::getline(consoleData, temp)) {
            names[count] = temp;
            std::getline(consoleData, temp);
            prices[count] = std::stoi(temp);
            count++;
        }
    }

    consoleData.close();
}

void showArrays (std::string consoles[], int prices[], int count) {
    std::cout << std::left << std::setw(40) << "Console";
    std::cout << std::right << std::setw(10) << "Price" << std::endl;
    for (int i = 0; i < count; i++) {
        std::cout << std::left << std::setw(40) << consoles[i];
        std::cout << std::right << std::setw(10) << prices[i] << std::endl;
    }

    std::cout << std::endl;
}

void lookUpPrice (std::string consoles[], int prices[], int count) {
    bool found = false;
    int i = 0;
    std::string console;

    std::cout << "Console name? ";
    std::cin.ignore();
    std::getline(std::cin, console);

    while (!found && i < count) {
        if (console == consoles[i]) {
            found = true;
            std::cout << "The current price for " << console 
                      << " is " << prices[i] << std::endl;
        } else {
            i++;
        }
    }

    if (!found) {
        std::cout << "Console not found. Name must match exactly.\n";
    }

    std::cout << std::endl;
}

void sortPrices (std::string consoles[], int prices[], int count) {
    int highest;
    // int start;
    int tempInt;
    std::string tempStr;

    for (int i = 0; i < count; i++) {
        highest = i;
        for (int j = i + 1; j < count; j++) {
            if (prices[j] > prices[highest]) {
                highest = j;
            }
        }
        if (highest != i) {
            // swaps value at current position with new highest value
            tempInt = prices[i];
            prices[i] = prices[highest];
            prices[highest] = tempInt;

            // keeps console names in sync with prices
            tempStr = consoles[i];
            consoles[i] = consoles[highest];
            consoles[highest] = tempStr;
        }
    }

    std::cout << "Consoles sorted from highest price to lowest.\n" << std::endl;
}

void highestPrice (std::string consoles[], int prices[], int count) {
    int i;
    int highest = prices[0];
    std::string highestConsole = consoles[0];

    for (i = 0; i < count; i++) {
        if (highest < prices[i]) {
            highest = prices[i];
            highestConsole = consoles[i];
        }
    }
    
    std::cout << "The " << highestConsole << " has the highest price of "
              << highest << ".\n" << std::endl;
}

int showMenu () {
    int choice;
    
    std::cout << "Console Pricing - Main Menu\n" << std::endl;

    std::cout << "1. Display all console prices\n";
    std::cout << "2. Display price of specific console\n";
    std::cout << "3. Sort prices in descending order\n";
    std::cout << "4. Display console with the highest price\n" << std::endl;

    std::cout << "5. Exit the program\n" << std::endl;

    do {
        std::cout << "> ";
        std::cin >> choice;
        if (choice > 5 || choice < 1) {
            std::cout << "Invalid selection.\n";
        }
    } while (choice > 5 || choice < 1);
    
    return choice;
}
