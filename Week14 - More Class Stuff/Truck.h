#ifndef TRUCK_H
#define TRUCK_H
#include "Vehicle.h"

class Truck : public Vehicle {
    private:
        int capacity;
    public:
        Truck ();
        Truck (int, std::string, int);

        int getCapacity ();

        void setCapacity (int);

        void displayInfo ();
};

#endif