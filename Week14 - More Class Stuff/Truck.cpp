#include <iostream>
#include "Truck.h"

Truck::Truck () : Vehicle () {
    capacity = 0;
}

Truck::Truck (int towCap, std::string manu, int year) : Vehicle (manu, year) {
    capacity = towCap;
}

int Truck::getCapacity () {
    return capacity;
}

void Truck::setCapacity (int towCap) {
    capacity = towCap;
}

void Truck::displayInfo () {
    Vehicle::displayInfo ();
    std::cout << "Towing Capacity: " << capacity << std::endl;
}