#include <iostream>
#include "Car.h"

Car::Car () : Vehicle () {
    doors = 2;
}

Car::Car (int numDoors, std::string manu, int year) : Vehicle (manu, year) {
    doors = numDoors;
}

int Car::getDoors () {
    return doors;
}

void Car::setDoors (int numDoors) {
    doors = numDoors;
}

void Car::displayInfo () {
    Vehicle::displayInfo ();
    std::cout << "Doors: " << doors << std::endl;
}