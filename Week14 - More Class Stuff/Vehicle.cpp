#include <iostream>
#include "Vehicle.h"

Vehicle::Vehicle () {
    manufacturer = "";
    year = 0;
}

Vehicle::Vehicle (std::string manu, int vYear) {
    manufacturer = manu;
    year = vYear;
}

std::string Vehicle::getManu () {
    return manufacturer;
}

int Vehicle::getYear () {
    return year;
}

void Vehicle::setManu (std::string manu) {
    manufacturer = manu;
}

void Vehicle::setYear (int vYear) {
    year = vYear;
}

void Vehicle::displayInfo () {
    std::cout << "Vehicle Information:" << std::endl;
    std::cout << "Manufacturer: " << manufacturer << std::endl;
    std::cout << "Year Built: " << year << std::endl;
}