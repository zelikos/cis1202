#ifndef VEHICLE_H
#define VEHICLE_H

class Vehicle {
    private:
        std::string manufacturer;
        int year;
    public:
        Vehicle ();
        Vehicle (std::string, int);

        std::string getManu ();
        int getYear ();

        void setManu (std::string);
        void setYear (int);

        void displayInfo ();
};

#endif