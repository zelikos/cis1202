// Patrick Csikos
// CIS 1202 502
// Date

#include <iomanip>
#include <iostream>
#include "Vehicle.h"
#include "Car.h"
#include "Truck.h"
using namespace std;

void clearBuffer();
double getNum();

void getInfo (Vehicle*);

int main () {
    Vehicle *vehicle = new Vehicle ();
    Car *car = new Car ();
    Truck *truck = new Truck ();
    int doors, towCap;

    cout << "Vehicle:" << endl;
    getInfo (vehicle);
    cout << endl;
    vehicle->displayInfo();
    cout << endl;

    cout << "Car:" << endl;
    getInfo (car);
    cout << "Enter the number of doors: ";
    doors = getNum();
    car->setDoors (doors);
    cout << endl;
    car->displayInfo();
    cout << endl;

    cout << "Truck:" << endl;
    getInfo (truck);
    cout << "Enter the towing capacity: ";
    towCap = getNum ();
    truck->setCapacity (towCap);
    cout << endl;
    truck->displayInfo();
    cout << endl;

    delete vehicle;
    delete car;
    delete truck;

    return 0;
}

void getInfo (Vehicle* vehicle) {
    string manu;
    int year;

    cout << "Enter manufacturer: ";
    getline(cin, manu);
    vehicle->setManu (manu);

    cout << "Enter year built: ";
    year = getNum();
    vehicle->setYear (year);
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}

double getNum () {
    double validNum;
    bool valid = false;

    while(!valid) {
        cin >> validNum;

        if (cin.fail()) {
            cout << "Entry must be a number.\n" << "> ";
        } else {
            valid = true;
        }

        clearBuffer();
    }

    return validNum;
}