// Port of blank.cpp in Rust

use std::io;
use std::io::Write;

fn main() {
    let mut choice;
    
    loop {
        menu();
        choice = get_num() as u32;
    }
}

fn get_num () -> f32 {
    let valid_num: f32;

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read input.");

        let input: f32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Entry must be a number.");
                continue;
            }
        };

        valid_num = input;
        break;
    }

    valid_num
}

fn flush_buffer () {
    std::io::stdout().flush().ok().expect("Failed to flush buffer.");
}

fn menu () {
    println!("");
    println!();
    print!("> ");
    flush_buffer();
}


