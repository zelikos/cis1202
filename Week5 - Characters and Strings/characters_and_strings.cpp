// Patrick Csikos
// CIS 1202 502
// 2020-09-26

#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>

bool valid (char[]); // to check for exactly 17 numbers/uppercase letters, minus IOQZ
void origin (char[], char[]); // (VIN, origin)
int year (char[]);

const int SIZE = 16;

int main () {
    // Invalid VINs for testing
    // char VIN[] = "IFTRW14W84KC76110";
    // char VIN[] = "1FTR14W8KC7610";
    // char VIN[] = "1FTRW14W84#C76110";
    // char VIN[] = "1FtRW14w84Kc76110";
    // char VIN[] = "potato";

    // Valid VIN for testing
    // char VIN[] = "1FTRW14W8SKC76110";
    
    char VIN[] = "1FTRW14W84KC76110";
    char originName[SIZE];

    std::cout << "Testing VIN " << VIN << '\n';

    if (valid (VIN)) {
        std::cout << "VIN is valid\n";

        origin (VIN, originName);
        std::cout << "Origin: " << originName << '\n';
        std::cout << "Year: " << year (VIN) << '\n';
    } else {
        std::cout << "VIN is invalid\n";
    }

    return 0;
}

bool valid (char vNum[]) {
    bool isValid = true;
    int pos = 0;

    while (vNum[pos] != '\0') {
        // check if char isn't alphanumeric
        if (!isalnum(vNum[pos])) {
            isValid = false;
        // check for lowercase letters; lets digits through
        } else if (isalpha(vNum[pos]) && islower(vNum[pos])) {
            isValid = false;
        }
        
        switch (vNum[pos]) {
            case 'I':
            case 'O':
            case 'Q':
            case 'Z':
                isValid = false;
                break;
        }
        
        pos++;
    }

    if (pos != 17) {
        isValid = false;
    }

    return isValid;
}

void origin (char vNum[], char vehicleOrigin[]) {
    const int ORIGIN = 0; // vehicle origin ID'd at position 1
    const int NUM_CODES = 6; // 6 possible countries of origin

    // possible codes for each country
    const char CODES[NUM_CODES][SIZE] = {"ABCDEFGH",
                                         "JKLMNPQR",
                                         "STUVWXYZ",
                                         "12345",
                                         "67",
                                         "890"};

    // countries corresponding to the respective set of codes
    const char COUNTRIES[NUM_CODES][SIZE] = {"Africa",
                                             "Asia",
                                             "Europe",
                                             "North America",
                                             "Oceania",
                                             "South America"};

    // check each country's set of origin codes
    for (int i = 0; i < NUM_CODES; i++) {
        int j = 0;
        // check each code for respective country
        while (CODES[i][j] != '\0') {
            if (CODES[i][j] == vNum[ORIGIN]) {
                std::strncpy(vehicleOrigin, COUNTRIES[i], SIZE);
            }
            j++;
        }
    }
}

int year (char vNum[]) {
    const int YEAR_ID = 9; // vehicle year ID'd at position 10, index 9
    int year;

    if (vNum[YEAR_ID] >= 'P' && vNum[YEAR_ID] <= 'Y') {
        year = 1993 + (int(vNum[YEAR_ID]) - int('P'));
        if (vNum[YEAR_ID] != 'P') {
            year -= 1; // accounts for Q not being a valid option
        }
    } else if (vNum[YEAR_ID] >= '1' && vNum[YEAR_ID] <= '9') {
        year = 2001 + (int(vNum[YEAR_ID] - int('1')));
    } else if (vNum[YEAR_ID] >= 'A' && vNum[YEAR_ID] <= 'L') {
        year = 2010 + (int(vNum[YEAR_ID] - int('A')));
    } else {
        year = 0;
    }

/*
p 1993
q invalid char
r 1994
s 1995
t 1996
u 1997
v 1998
w 1999
x 2000
y 2001 <- range should end at 2000?
*/

    return year;
}
