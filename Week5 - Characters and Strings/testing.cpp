// Patrick Csikos
// CIS 1202 502
// Date

#include <fstream>
#include <iomanip>
#include <iostream>

int main () {
    std::string nums = "3847 384 00011";
    int numsInt = std::stoi(nums);

    std::cout << numsInt << '\n';
    return 0;
}
