#include <iomanip>
#include <iostream>
#include "Publication.h"

void Publication::storePublication (std::string itemTitle, std::string itemPublisher, double itemPrice,
                                    int itemYear, PublicationType itemType, int itemStock) {
    title = itemTitle;
    publisher = itemPublisher;
    price = itemPrice;
    year = itemYear;
    type = itemType;
    stock = itemStock;
}

Publication::Publication () {
    title = "";
    publisher = "";
    price = 0.0;
    year = 0;
    type = BOOK;
    stock = 0;
}

std::string Publication::pubTypeToString (PublicationType type) {
    std::string pubType;

    switch (type) {
        case BOOK:
            pubType = "Book";
            break;
        case MAGAZINE:
            pubType = "Magazine";
            break;
        case NEWSPAPER:
            pubType = "Newspaper";
            break;
        case AUDIO:
            pubType = "Audio";
            break;
        case VIDEO:
            pubType = "Video";
            break;
    }

    return pubType;
}

void Publication::displayInfo () {
    std::cout << title << std::endl;
    std::cout << publisher << std::endl;
    std::cout << "$ " << std::fixed << std::setprecision(2) << price << std::endl;
    std::cout << year << std::endl;
    std::cout << pubTypeToString (type) << std::endl;
    std::cout << stock << " in stock" << std::endl;
}

void Publication::checkOut () {
    if (stock > 0) {
        stock--;
        std::cout << title << " successfully checked out." << std::endl;
        std::cout << "Stock: " << stock << std::endl;
    } else {
        std::cout << title << " out of stock." << std::endl;
    }
}

void Publication::checkIn () {
    stock++;
    std::cout << title << " successfully checked in." << std::endl;
    std::cout << "Stock: " << stock << std::endl;
}

std::string Publication::getTitle () {
    return title;
}

int Publication::getStock () {
    return stock;
}
