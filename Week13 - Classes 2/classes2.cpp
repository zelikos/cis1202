// Patrick Csikos
// CIS 1202 502
// 2020-11-16

#include <fstream>
#include <iomanip>
#include <iostream>
#include "Publication.h"
using namespace std;

enum Option { DISPLAY, CHECKIN, CHECKOUT };

void clearBuffer();
double getNum();

PublicationType getPubType();
void getPublicationInfo (Publication&);

void getPublications (Publication [], int&);
void showPublications (Publication [], int);
void showTitles (Publication [], int);
int findPublication (Publication [], int, string);

void searchPublications (Publication [], int, Option);

void displayMenu ();

const int SIZE = 10;

int main () {
    Publication publications[SIZE];
    int numPublications = 0;
    int choice;

    getPublications (publications, numPublications);

    do {
        displayMenu ();
        cout << "> ";
        choice = getNum ();

        switch (choice) {
            case 1:
                showPublications (publications, numPublications);
                break;
            case 2:
                showTitles (publications, numPublications);
                break;
            case 3:
                searchPublications (publications, numPublications, DISPLAY);
                break;
            case 4:
                searchPublications (publications, numPublications, CHECKOUT);
                break;
            case 5:
                searchPublications (publications, numPublications, CHECKIN);
                break;
            case 6:
                cout << "Exiting..." << endl;
                break;
            default:
                cout << "Selection must be a number 1-6\n" << endl;
                break;
        }
    } while (choice != 6);

    return 0;
}

void getPublications (Publication pubs[], int &numPubs) {
    string title, author, temp;
    double price;
    PublicationType type;
    int year, stock;
    ifstream f;

    f.open ("publications.txt");

    while (getline(f, title)) {
        getline(f, author);

        getline(f, temp);
        price = stod(temp);

        getline(f, temp);
        year = stoi(temp);

        getline(f, temp);
        type = static_cast<PublicationType> (stoi(temp));

        getline(f, temp);
        stock = stoi(temp);

        pubs[numPubs].storePublication (title, author, price, year, type, stock);

        numPubs++;
    }

    f.close ();
}

void showPublications (Publication pubs[], int numPubs) {
    for (int i = 0; i < numPubs; i++) {
        pubs[i].displayInfo ();
        cout << endl;
    }
}

void showTitles (Publication pubs[], int numPubs) {
    for (int i = 0; i < numPubs; i++) {
        cout << pubs[i].getTitle () << endl;;
    }

    cout << endl;
}

int findPublication (Publication pubs[], int numPubs, string pub) {
    int pos = -1;
    string title;

    for (int i = 0; i < numPubs; i++) {
        title = pubs[i].getTitle();
        if (pub.compare(title) == 0) {
            pos = i;
        }
    }

    if (pos == -1) {
        cout << "Publication not found." << endl;
    }

    return pos;
}

void searchPublications (Publication pubs[], int numPubs, Option option) {
    string pub;
    cout << "Publication title: ";
    getline (cin, pub);

    int position = findPublication (pubs, numPubs, pub);
    if (position != -1) {
        switch (option) {
            case DISPLAY:
                pubs[position].displayInfo();
                break;
            case CHECKIN:
                pubs[position].checkIn();
                break;
            case CHECKOUT:
                pubs[position].checkOut();
                break;
        }
    }

    cout << endl;
}

void displayMenu () {
    cout << "i. Display all publications\n";
    cout << "ii. Display publication titles\n";
    cout << "iii. Find a publication\n";
    cout << "iv. Check out\n";
    cout << "v. Check in\n";
    cout << "vi. Exit\n" << endl;
}

void getPublicationInfo (Publication &publication) {
    string title, publisher;
    double price;
    int year, stock;
    PublicationType type;

    cout << "Title: ";
    getline(cin, title);

    cout << "Publisher: ";
    getline(cin, publisher);

    cout << "Price: ";
    price = getNum();

    cout << "Year: ";
    year = getNum();

    cout << "1. Book / 2. Magazine / 3. Newspaper / 4. Audio / 5. Video" << endl;;
    type = getPubType();

    cout << "Stock: ";
    stock = getNum();

    publication.storePublication(title, publisher, price, year, type, stock);
}

PublicationType getPubType() {
    PublicationType type;
    int choice;

    do {
        cout << "Type: ";
        choice = getNum();

        switch (choice) {
            case 1:
                type = BOOK;
                break;
            case 2:
                type = MAGAZINE;
                break;
            case 3:
                type = NEWSPAPER;
                break;
            case 4:
                type = AUDIO;
                break;
            case 5:
                type = VIDEO;
                break;
            default:
                cout << "Selection must be 1, 2, 3, 4, or 5." << endl;
                break;
        }
    } while (choice < 1 || choice > 5);

    return type;
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}

double getNum () {
    double validNum;
    bool valid = false;

    while(!valid) {
        cin >> validNum;

        if (cin.fail()) {
            cout << "Entry must be a number.\n" << "> ";
        } else {
            valid = true;
        }

        clearBuffer();
    }

    return validNum;
}
