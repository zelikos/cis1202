// Patrick Csikos
// CIS 1202 502
// 2020-10-12

#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
using namespace std;

// Main Menu options
enum Options { NEW = 1, LOAD = 2, QUIT = 3};

struct Expense {
    string name;
    double amount;
};

struct Expenses {
    string month;
    vector<Expense> expenses;
    double budget;
    double total;
    bool saved;
};

struct ExpensePos {
    bool found;
    int pos;
};

// Utility functions
void clearBuffer();
double getNum();
string makeLower(string);

// Main Menu
int mainMenu ();
Expenses loadExpenses(Options);

// Expenses Submenu
void expensesMenu (Expenses*);
void addExpense (Expenses*);
ExpensePos findExpense (Expenses*, string);
void removeExpense (Expenses*);
void displayExpenses (Expenses*);
void sortByValue (Expenses*);
void sortByName (Expenses*);
void saveExpenses (Expenses*);

int main () {
    int choice;
    Expenses expenses;

    do {
        choice = mainMenu();
        switch (choice) {
            case NEW:
                expenses = loadExpenses(NEW);
                expensesMenu(&expenses);
                break;
            case LOAD:
                expenses = loadExpenses(LOAD);
                if (expenses.saved == true) { // Check for successful load
                    expensesMenu(&expenses);
                }
                break;
            case QUIT:
                cout << "Exiting..." << endl;
                break;
            default:
                cout << "Valid options: 1, 2, 3\n" << endl;
        }
    } while (choice != QUIT);

    return 0;
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}

double getNum () {
    double validNum;
    bool valid = false;

    while(!valid) {
        cin >> validNum;

        if (cin.fail()) {
            cout << "Entry must be a number.\n" << "> ";
        } else {
            valid = true;
        }
        clearBuffer();
    }

    return validNum;
}

// Makes strings lowercase for search terms so they don't need to be case-sensitive
string makeLower (string toMakeLower) {
    string loweredString;
    for (int i = 0; i < toMakeLower.length(); i++) {
        loweredString += tolower(toMakeLower[i]);
    }

    return loweredString;
}

int mainMenu () {
    int choice;

    cout << "Monthly Budget and Expenses Tracker\n" << endl;

    cout << "1. New Month\n";
    cout << "2. Load Existing Month\n";
    cout << "3. Quit\n" << endl;

    cout << "> ";

    choice = getNum();

    return choice;
}

Expenses loadExpenses(Options option) {
    Expenses expenses;

    if (option == NEW) {
        cout << "Month to track: ";
        getline(cin, expenses.month);

        expenses.budget = 0;
        expenses.total = 0;
        expenses.saved = false;
    } else if (option == LOAD) {
        ifstream inFile;
        string loadMonth;
        string amount;
        vector<string> expenseValues;
        Expense loadedExpense;

        cout << "Filename to load: ";
        getline(cin, loadMonth);

        inFile.open(loadMonth + ".txt");
        if (!inFile) {
            cout << "File not found. Name must match exactly, sans file extension.\n";
            cout << "E.G. Enter 'October' to load 'October.txt'\n" << endl;
        } else {
            // loads members not part of vector
            getline(inFile, expenses.month);
            getline(inFile, amount);
            expenses.budget = stod(amount);
            getline(inFile, amount);
            expenses.total = stod(amount);

            // loads expenses vector
            while (getline(inFile, amount)) {
                expenseValues.push_back(amount);
            }

            for (int i = 0; i < expenseValues.size(); i++) {
                loadedExpense.name = expenseValues[i];
                i++;
                loadedExpense.amount = stod(expenseValues[i]);
                expenses.expenses.push_back(loadedExpense);
            }

            expenses.saved = true;
            cout << "Successfully loaded." << endl;
        }

        inFile.close();
    }

    return expenses;
}

void expensesMenu (Expenses *expenses) {
    int choice;
    char toSave;

    do {
        cout << expenses->month << " Expenses\n" << endl;
        cout << "1. Set Budget\n";
        cout << "2. Add Expense\n";
        cout << "3. Remove Expense\n";
        cout << "4. Display Expenses\n";
        cout << "5. Sort Expenses by Amount\n";
        cout << "6. Sort Expenses by Name\n";
        cout << "7. Save Expenses to file\n";
        cout << "8. Main Menu\n" << endl;

        cout << fixed << showpoint << setprecision(2);
        cout << "Budget: $" << expenses->budget << endl;
        cout << "Total expenses: $" << expenses->total << endl;
        if (expenses->total > expenses->budget) {
            cout << "$" << (expenses->total - expenses->budget) << " over budget" << endl;
        } else if (expenses->budget > expenses->total) {
            cout << "$" << (expenses->budget - expenses->total) << " remaining in budget" << endl;
        }
    
        cout << "> ";

        choice = getNum();
        switch (choice) {
            case 1:
                cout << "Budget amount: ";
                expenses->budget = getNum();
                cout << endl;
                break;
            case 2:
                addExpense (expenses);
                break;
            case 3:
                if (expenses->expenses.size() == 0) {
                    cout << "No expenses available to remove.\n";
                } else {
                    removeExpense (expenses);
                }
                break;
            case 4:
                displayExpenses (expenses);
                break;
            case 5:
                sortByValue (expenses);
                break;
            case 6:
                sortByName (expenses);
                break;
            case 7:
                saveExpenses (expenses);
                break;
            case 8:
                if (expenses->saved == false) {
                    cout << "Save current expenses to a file? Y/N > ";
                
                    cin.get(toSave);
                    clearBuffer();

                    if (tolower(toSave) == 'y') {
                        saveExpenses (expenses);
                    }
                }
                break;
            default:
                cout << "Invalid selection.\n" << endl;
                break;
        }
    } while (choice != 8);

}

void addExpense (Expenses *expenses) {
    Expense newExpense;

    cout << "Name of expense: ";
    getline(cin, newExpense.name);
    cout << "Amount: ";
    newExpense.amount = getNum();

    expenses->expenses.push_back(newExpense);

    expenses->total += newExpense.amount;
}

ExpensePos findExpense (Expenses *expenses, string item) {
    ExpensePos found;
    found.found = false;

    string compareTo;
    
    int i = 0, numExpenses = expenses->expenses.size();;

    while (i < numExpenses && !(found.found)) {
        compareTo = makeLower(expenses->expenses[i].name);
        if (item.compare(compareTo) == 0) {
            found.pos = i;
            found.found = true;
        }
        i++;
    }

    return found;
}

void removeExpense (Expenses *expenses) {
    ExpensePos expenseToRemove;
    string item;

    cout << "Name of expense to remove: ";
    getline(cin, item);
    expenseToRemove = findExpense (expenses, makeLower(item));

    if (expenseToRemove.found) {
        expenses->total -= expenses->expenses[expenseToRemove.pos].amount;
        expenses->expenses.erase(expenses->expenses.begin() + expenseToRemove.pos);
    } else {
        cout << "Expense not found.\n";
    }

    expenses->saved = false;
    cout << endl;
}

void displayExpenses (Expenses *expenses) {
    int numExpenses = expenses->expenses.size();

    if (numExpenses == 0) {
        cout << "No expenses entered." << endl;
    } else {
        cout << left << setw(32) << "Expense";
        cout << right << setw(16) << "Amount" << endl;
        for (int i = 0; i < numExpenses; i++) {
            cout << left << setw(32) << expenses->expenses[i].name;
            cout << right << setw(16) << expenses->expenses[i].amount;
            cout << endl;
        }
        cout << endl;
    }
}

void sortByValue (Expenses *expenses) {
    Expense temp;
    int changedPos, option;
    int numExpenses = expenses->expenses.size();

    cout << "1. Low -> High\n";
    cout << "2. High -> Low\n";

    option = getNum();

    switch (option) {
        case 1: // sort by ascending value
            for (int i = 0; i < numExpenses; i++) {
                changedPos = i;
                for (int j = i+1; j < numExpenses; j++) {
                    if (expenses->expenses[j].amount < expenses->expenses[changedPos].amount) {
                        changedPos = j;
                    }
                }
                if (changedPos != i) {
                    temp = expenses->expenses[i];
                    expenses->expenses[i] = expenses->expenses[changedPos];
                    expenses->expenses[changedPos] = temp;
                }
            }
            break;

        case 2: // sort by descending value
            for (int i = 0; i < numExpenses; i++) {
                changedPos = i;
                for (int j = i+1; j < numExpenses; j++) {
                    if (expenses->expenses[j].amount > expenses->expenses[changedPos].amount) {
                        changedPos = j;
                    }
                }
                if (changedPos != i) {
                    temp = expenses->expenses[i];
                    expenses->expenses[i] = expenses->expenses[changedPos];
                    expenses->expenses[changedPos] = temp;
                }
            }
            break;
        default:
            cout << "Invalid selection.\n";
            break;
    }

    expenses->saved = false;
}

void sortByName (Expenses *expenses) {
    Expense temp;
    int changedPos, option;
    int numExpenses = expenses->expenses.size();

    cout << "1. A->Z\n";
    cout << "2. Z->A\n";

    option = getNum();

    switch (option) {
        case 1: // alphabetical sort
            for (int i = 0; i < numExpenses; i++) {
                changedPos = i;
                for (int j = i+1; j < numExpenses; j++) {
                    if (expenses->expenses[j].name < expenses->expenses[changedPos].name) {
                        changedPos = j;
                    }
                }
                if (changedPos != i) {
                    temp = expenses->expenses[i];
                    expenses->expenses[i] = expenses->expenses[changedPos];
                    expenses->expenses[changedPos] = temp;
                }
            }
            break;

        case 2: // reverse alphabetical sort
            for (int i = 0; i < numExpenses; i++) {
                changedPos = i;
                for (int j = i+1; j < numExpenses; j++) {
                    if (expenses->expenses[j].name > expenses->expenses[changedPos].name) {
                        changedPos = j;
                    }
                }
                if (changedPos != i) {
                    temp = expenses->expenses[i];
                    expenses->expenses[i] = expenses->expenses[changedPos];
                    expenses->expenses[changedPos] = temp;
                }
            }
            break;
        default:
            cout << "Invalid selection.\n";
            break;
    }

    expenses->saved = false;
}

void saveExpenses (Expenses *expenses) {
    ofstream outFile;

    outFile.open(expenses->month + ".txt");
    outFile << expenses->month << '\n';
    outFile << expenses->budget << '\n';
    outFile << expenses->total << '\n';
    for (int i = 0; i < expenses->expenses.size(); i++) {
        outFile << expenses->expenses[i].name << '\n';
        outFile << expenses->expenses[i].amount << '\n';
    }

    outFile.close();
    cout << "Expenses saved to " << expenses->month << ".txt" << endl;
    expenses->saved = true;
}