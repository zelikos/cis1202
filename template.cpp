// Patrick Csikos
// CIS 1202 502
// Date

#include <iomanip>
#include <iostream>
using namespace std;

void clearBuffer();
double getNum();

int main () {
    
    return 0;
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}

double getNum () {
    double validNum;
    bool valid = false;

    while(!valid) {
        cin >> validNum;

        if (cin.fail()) {
            cout << "Entry must be a number.\n" << "> ";
        } else {
            valid = true;
        }
        
        clearBuffer();
    }
    
    return validNum;
}