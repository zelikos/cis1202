// Patrick Csikos
// CIS 1202 502
// 2020-10-02

#include <iomanip>
#include <iostream>

struct WinLoss {
    int wins;
    int losses;
};

struct Team {
    std::string name;
    WinLoss home;
    WinLoss away;
    WinLoss total;
};

int getInt();

int menu();
WinLoss getWinLoss(std::string);
Team getTeam();
void displayWinLoss(WinLoss);
void displayTeam(Team);
void displayAllTeams(Team[], int);
void findTeam(Team[], int);

int main () {
    const int SIZE = 30;
    Team teams[SIZE];
    int choice;
    int numTeams = 0;

    do {
        choice = menu ();
        switch (choice) {
            case 1:
                if (numTeams == SIZE) {
                    std::cout << "Max number of teams entered.\n";
                } else {
                    teams[numTeams] = getTeam();
                    numTeams++;
                }
                break;
            case 2:
                if (numTeams == 0) {
                    std::cout << "No teams entered.\n";
                } else {
                    displayAllTeams (teams, numTeams);
                }
                break;
            case 3:
                if (numTeams == 0) {
                    std::cout << "No teams entered.\n";
                } else {
                    findTeam (teams, numTeams);
                }
                break;
            case 4:
                std::cout << "Exiting...\n";
                break;
        }
    } while (choice != 4);
    
    
    return 0;
}

int getInt () {
    int validInt;
    bool valid = false;

    while(!valid) {
        std::cin >> validInt;

        if (std::cin.fail()) {
            std::cout << "Entry must be an integer: ";
        } else {
            valid = true;
        }

        std::cin.clear();
        std::cin.ignore(__INT_MAX__, '\n');
    }
    
    return validInt;
}

int menu () {
    int choice;

    std::cout << "1. Enter a new team\n";
    std::cout << "2. Display all teams\n";
    std::cout << "3. Display a particular team\n";
    std::cout << "4. Exit\n" << std::endl;

    do {
        std::cout << "> ";
        choice = getInt();

        if (choice > 4 || choice < 1) {
            std::cout << "Valid options: 1, 2, 3, 4\n";
        }
    } while (choice > 4 || choice < 1);

    std::cout << std::endl;
    return choice;
}

WinLoss getWinLoss (std::string venue) {
    WinLoss record;

    std::cout << venue << " wins: ";
    record.wins = getInt();

    std::cout << venue << " losses: ";
    record.losses = getInt();

    return record;
}

Team getTeam () {
    Team newTeam;

    std::cout << "Team name: ";
    std::getline(std::cin, newTeam.name);

    newTeam.home = getWinLoss ("Home");
    newTeam.away = getWinLoss ("Away");

    newTeam.total.wins = newTeam.home.wins + newTeam.away.wins;
    newTeam.total.losses = newTeam.home.losses + newTeam.away.losses;

    std::cout << std::endl;
    return newTeam;
}

void displayWinLoss (WinLoss record) {
    std::cout << record.wins << "-" << record.losses;
}

void displayTeam (Team team) {
    std::cout << team.name << '\n';
    std::cout << "Total record: "; displayWinLoss (team.total); std::cout << '\n';
    std::cout << "Home record: "; displayWinLoss (team.home); std::cout << '\n';
    std::cout << "Away record: "; displayWinLoss (team.away); std::cout << '\n';
    std::cout << std::endl;

}

void displayAllTeams (Team teams[], int numTeams) {
    for (int i = 0; i < numTeams; i++) {
        displayTeam (teams[i]);
    }
}

void findTeam (Team teams[], int numTeams) {
    std::string team;
    bool found = false;

    std::cout << "Team name? ";
    std::getline (std::cin, team);

    for (int i = 0; i < numTeams; i++) {
        if (teams[i].name == team) {
            displayTeam (teams[i]);
            found = true;
        }
    }

    if (!found) {
        std::cout << "Team not found.\n";
    }
}