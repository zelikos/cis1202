#include <iostream>
#include "Publication.h"

void Publication::storePublication (std::string itemTitle, std::string itemPublisher, double itemPrice,
                                    int itemYear, PublicationType itemType, int itemStock) {
    title = itemTitle;
    publisher = itemPublisher;
    price = itemPrice;
    year = itemYear;
    type = itemType;
    stock = itemStock;
}

Publication::Publication () {
    title = "";
    publisher = "";
    price = 0.0;
    year = 0;
    type = BOOK;
    stock = 0;
}

void Publication::displayInfo () {
    std::cout << title << std::endl;
    std::cout << publisher << std::endl;
    std::cout << price << std::endl;
    std::cout << year << std::endl;
    std::cout << type << std::endl;
    std::cout << stock << std::endl;
}

void Publication::checkOut () {
    if (stock > 0) {
        stock--;
    } else {
        std::cout << "Out of stock." << std::endl;
    }
}

void Publication::checkIn () {
    stock++;
}

std::string Publication::getTitle () {
    return title;
}

int Publication::getStock () {
    return stock;
}