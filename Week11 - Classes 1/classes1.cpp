// Patrick Csikos
// CIS 1202 502
// 2020-11-05

#include <iomanip>
#include <iostream>
#include <memory>
#include "Publication.h"
using namespace std;

void clearBuffer();
double getNum();

PublicationType getPubType();

void getPublicationInfo (Publication*);

int main () {
    Publication * book = new Publication();

    getPublicationInfo (book);

    cout << "Publication title: " << book->getTitle() << endl;
    book->checkOut();
    cout << book->getStock() << " in stock" << endl;
    book->checkIn();
    cout << book->getStock() << " in stock" << endl;

    delete book;
    return 0;
}

void getPublicationInfo (Publication *publication) {
    string title, publisher;
    double price;
    int year, stock;
    PublicationType type;

    cout << "Title: ";
    getline(cin, title);

    cout << "Publisher: ";
    getline(cin, publisher);

    cout << "Price: ";
    price = getNum();

    cout << "Year: ";
    year = getNum();

    cout << "1. Book / 2. Magazine / 3. Newspaper / 4. Audio / 5. Video" << endl;;
    type = getPubType();

    cout << "Stock: ";
    stock = getNum();

    publication->storePublication(title, publisher, price, year, type, stock);
}

PublicationType getPubType() {
    PublicationType type;
    int choice;

    do {
        cout << "Type: ";
        choice = getNum();

        switch (choice) {
            case 1:
                type = BOOK;
                break;
            case 2:
                type = MAGAZINE;
                break;
            case 3:
                type = NEWSPAPER;
                break;
            case 4:
                type = AUDIO;
                break;
            case 5:
                type = VIDEO;
                break;
            default:
                cout << "Selection must be 1, 2, 3, 4, or 5." << endl;
                break;
        }
    } while (choice < 1 || choice > 5);

    return type;
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}

double getNum () {
    double validNum;
    bool valid = false;

    while(!valid) {
        cin >> validNum;

        if (cin.fail()) {
            cout << "Entry must be a number.\n" << "> ";
        } else {
            valid = true;
        }

        clearBuffer();
    }

    return validNum;
}