#ifndef ENTITY_H
#define ENTITY_H
#include <string>

class Entity {
    private:
        std::string entityName;
        int hitPoints;
        int attack;
        int defense;
        int magAttack;
        int magDefense;
    public:
        Entity (std::string);
        Entity (std::string, int, int, int, int, int);

        std::string getName () { return entityName; }
        int getHP () { return hitPoints; }
        int getAtt () { return attack; }
        int getDef () { return defense; }
        int getMagAtt () { return magAttack; }
        int getMagDef () { return magDefense; }

        void setHP (int hp) { hitPoints = hp; }
        void setAtt (int att) { attack = att; }
        void setDef (int def) { defense = def; }
        void setMagAtt (int mAtt) { magAttack = mAtt; }
        void setMagDef (int mDef) { magDefense = mDef; }

        void displayStats ();

        virtual int melee (std::string) = 0;
};

#endif