#ifndef GOLEM_H
#define GOLEM_H
#include "Entity.h"

class Golem : public Entity {
    public:
        Golem ();

        virtual int melee (std::string) override;
        int special (std::string);
};

#endif