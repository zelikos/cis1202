#include <iostream>
#include "Player.h"

Player::Player (std::string name) : Entity (name) {
    setHP (50);
    hasSword = false;
    hasSpell = false;
}

Player::Player (std::string name, int hp, bool sword, bool spell) : Entity (name) {
    setHP (hp);
    hasSword = sword;
    hasSpell = spell;
}

void Player::brokeSword () {
    std::cout << "The blade shatters to pieces!" << std::endl;
    hasSword = false;
}

int Player::melee (std::string target) {
    int damage;

    if (hasSword == false) {
        std::cout << "You throw a punch at " << target << "!" << std::endl;
        damage = getAtt ();
    } else if (hasSword == true) {
        std::cout << "You slash at " << target << " with your sword!" << std::endl;
        damage = (getAtt () + 5);
    }

    return damage;
}

int Player::lightning (std::string target) {
    int damage;

    std::cout << "You aim your outstretched hand at " << target
              << ", unleashing a large bolt of lightning!" << std::endl;
    damage = (getMagAtt () * 3);
    return damage;
}

void Player::prompt () {
    std::cout << "\nHP: " << getHP ();
    if (hasSword == true) {
        std::cout << " | " << "Weapon: Sword";
    }
    if (hasSpell == true) {
        std::cout << " | " << "Spell: Lightning";
    }
    std::cout << std::endl;
}
