#include <iostream>
#include <string>
#include "Entity.h"

Entity::Entity (std::string name) {
    entityName = name;
    hitPoints = 10;
    attack = 10;
    defense = 10;
    magAttack = 10;
    magDefense = 10;
}

Entity::Entity (std::string name, int hp, int att, int def, int magAtt, int magDef) {
    entityName = name;
    hitPoints = hp;
    attack = att;
    defense = def;
    magAttack = magAtt;
    magDefense = magDefense;
}

void Entity::displayStats () {
    std::cout << "Name: " << entityName << std::endl;
    std::cout << "HP: " << hitPoints << std::endl;
    std::cout << "ATT: " << attack << std::endl;
    std::cout << "DEF: " << defense << std::endl;
    std::cout << "MATT: " << magAttack << std::endl;
    std::cout << "MDEF: " << magDefense << std::endl;
}
