#ifndef GOBLIN_H
#define GOBLIN_H
#include "Entity.h"

class Goblin : public Entity {
    public:
        Goblin ();

        virtual int melee (std::string) override;
};

#endif