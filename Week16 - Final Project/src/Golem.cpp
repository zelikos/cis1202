#include <iostream>
#include "Golem.h"

Golem::Golem () : Entity ("Golem") {
    setHP (100);
    setAtt (20);
    setDef (100);
    setMagDef (1);
}

int Golem::melee (std::string name) {
    int damage = getAtt ();
    std::cout << "The " << name << " slams its arm into the ground,\n";
    std::cout << "sending shards of rock flying at you!" << std::endl;
    return damage;
}

int Golem::special (std::string name) {
    int damage;
    std::cout << "The " << name << " suddenly lurches forward, its rocky body collapsing on top of you!" << std::endl;
    damage = (getAtt () * 5);
    return damage;
}