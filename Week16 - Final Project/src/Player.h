#ifndef PLAYER_H
#define PLAYER_H
#include "Entity.h"

class Player : public Entity {
    private:
        bool hasSword;
        bool hasSpell;
    public:
        Player (std::string);
        Player (std::string, int, bool, bool);

        bool foundSword () { return hasSword; }
        bool foundSpell () { return hasSpell; }

        void getSword () { hasSword = true; }
        void getSpell () { hasSpell = true; }

        void brokeSword ();

        virtual int melee (std::string) override;
        int lightning (std::string);

        void prompt ();
};

#endif