// Patrick Csikos
// CIS 1202 502
// 2020-12-09

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <time.h>
#include "Player.h"
#include "Goblin.h"
#include "Golem.h"
using namespace std;

enum SceneID { FOREST,
               CAVE,
               FINALE
};

enum DamageType { MELEE, MAGIC };
enum TargetType { PLAYER, ENEMY };

const int LIMIT = 40;
struct SaveData {
    char playerName[LIMIT];
    int playerHP;
    bool playerSword;
    bool playerSpell;
    SceneID scene;
};

class dataNotFound {};

void playTest ();
void makeLower (string&);
void scenePause ();

bool criticalHit ();
void damageCalc (Entity*, int, DamageType, TargetType);

SaveData loadGame ();
void saveGame (Player*, SceneID);
void newGame ();

void displayMenu (bool);

void forestScene (Player*);
void goblinBattle (Player*);
void caveScene (Player*);
void golemBattle (Player*);
void finaleScene (Player*);

void gameOver ();

int main () {
    bool saveFound, tryAgain = false;
    string menuChoice;
    SaveData data;
    // playTest ();

    try {
        data = loadGame();
        saveFound = true;
    } catch (dataNotFound) {
        saveFound = false;
    }

    displayMenu (saveFound);

    do {
        cout << "\n> ";
        getline (cin, menuChoice);
        makeLower(menuChoice);
        if (menuChoice == "n" || menuChoice == "new") {
            newGame ();
            tryAgain = false;
        } else if (saveFound && menuChoice == "c" || menuChoice == "continue") {
            Player* player = new Player (data.playerName, data.playerHP,
                                         data.playerSword, data.playerSpell);
            cout << "Welcome back, " << player->getName() << ".\n" << endl;
            switch (data.scene) {
                case FOREST:
                    forestScene (player);
                    break;
                case CAVE:
                    caveScene (player);
                    break;
                case FINALE:
                    finaleScene (player);
                    break;
            }
            delete player;
            tryAgain = false;
        } else if (menuChoice == "q" || menuChoice == "quit") {
            cout << "Exiting..." << endl;
            tryAgain = false;
        } else {
            cout << "Invalid selection." << endl;
            tryAgain = true;
        }
    } while (tryAgain);

    return 0;
}

void makeLower (string &str) {
    transform(str.begin(), str.end(), str.begin(), ::tolower);
}

void scenePause () {
    string input;
    cout << "\nPress Enter to continue..." << endl;

    getline (cin, input);
}

bool criticalHit () {
    bool crit = false;
    srand (time(NULL));
    int num = rand() % 100;

    if (num <= 10) {
        crit = true;
    }

    return crit;
}

void damageCalc (Entity* target, int damage, DamageType dmgType, TargetType trgType) {
    int defense;

    switch (dmgType) {
        case MELEE:
            defense = target->getDef();
            break;
        case MAGIC:
            defense = target->getMagDef();
            break;
    }

    damage = damage - defense;
    if (damage <= 0) {
        damage = 1;
    }
    if (criticalHit()) {
        damage *= 2;
        cout << "Critical Hit!\n";
    }
    target->setHP(target->getHP() - damage);

    switch (trgType) {
        case PLAYER:
            cout << "You take " << damage << " damage!" << endl;
            break;
        case ENEMY:
            cout << target->getName() << " takes " << damage << " damage!" << endl;
            break;
    }
}

SaveData loadGame () {
    SaveData saveData;
    fstream saveFile ("save.dat", ios::in | ios::binary);
    if (!saveFile) {
        throw dataNotFound();
    } else {
        saveFile.read(reinterpret_cast<char *>(&saveData), sizeof(saveData));
    }

    saveFile.close();

    return saveData;
}

void saveGame (Player* player, SceneID scene) {
    SaveData saveData;
    fstream saveFile ("save.dat", ios::out | ios::trunc | ios::binary);

    strncpy(saveData.playerName, player->getName().c_str(), sizeof(saveData.playerName));
    saveData.playerHP = player->getHP();
    saveData.playerSword = player->foundSword();
    saveData.playerSpell = player->foundSpell();
    saveData.scene = scene;

    saveFile.write(reinterpret_cast<char *>(&saveData), sizeof(saveData));

    saveFile.close();

    cout << "We'll be awaiting your return, " << saveData.playerName << "." << endl;
}

void newGame () {
    string input;

    cout << "Darkness fills your vision." << endl;
    cout << "You see a small light source in the distance," << endl;
    cout << "and suddenly, a voice echoes through your mind.\n" << endl;

    cout << "\"Do you remember your name?\"" << endl;
    cout << "Enter your name: ";
    getline (cin, input);

    Player* player = new Player (input);

    cout << "\n\"" << player->getName() << "... I see." << endl;
    cout << "\n\"You will soon awaken in a forest." << endl;
    cout << "The forest is full of dangers; be wary of your surroundings." << endl;
    cout << "If you need a break and aren't in battle, you can 'quit' at any time.\n" << endl;

    cout << "\"Good luck, " << player->getName() << ".\"\n" << endl;

    forestScene (player);
    delete player;
}

void displayMenu (bool saveFound) {
    cout << "Some Adventure\n" << endl;
    cout << "[N] New Game" << endl;
    if (saveFound == true) {
        cout << "[C] Continue" << endl;
    }
    cout << "[Q] Quit" << endl;
}

void forestScene (Player* player) {
    bool tryAgain = true;
    string input;

    scenePause();

    cout << "\nYou awake in a dimly-lit forest, trees obscuring much of the sunlight above.\n";
    cout << "\nThere is a path leading 'north' through the forest.\n";

    if (player->foundSword() == false) {
        cout << "You notice a shiny object on the ground nearby.\n";
        cout << "Perhaps you should take a closer 'look' at it?\n";
    }

    cout << "\nWhat do you do?\n";
    do {
        cout << "> ";
        getline (cin, input);
        makeLower(input);
        if (input == "north") {
            cout << "You follow the path heading north." << endl;
            goblinBattle (player);
            tryAgain = false;
        } else if (input == "look") {
            cout << "As you approach, you realize that the object is a sword.\n";
            cout << "Its appearance is somewhat worn, but it seems to be in good shape.\n";
            cout << "You take the sword, carefully sliding it into your belt,\n";
            cout << "before following the northern path.\n" << endl;
            player->getSword();
            goblinBattle (player);
            tryAgain = false;
        } else if (input == "quit") {
            saveGame (player, FOREST);
            tryAgain = false;
        } else {
            cout << "No, that won't work..." << endl;
            tryAgain = true;
        }
    } while (tryAgain);
}

void goblinBattle (Player* player) {
    int damage;
    string input, target;

    scenePause();

    cout << "\nYou soon enter a clearing, with the path continuing northward.\n";
    cout << "Suddenly, you hear the sound of a rustling bush, and a goblin jumps out at you!\n";

    if (player->foundSword() == true) {
        cout << "You draw your sword, readying yourself for battle.\n";
    } else if (player->foundSword() == false) {
        cout << "Despite being unarmed, you have little choice but to defend yourself.\n";
    }

    cout << "\nThe goblin cackles as it approaches. Prepare to 'attack'!\n";

    Goblin* goblin = new Goblin();
    target = goblin->getName();

    while (player->getHP() > 0 && goblin->getHP() > 0) {
        player->prompt();
        cout << "> ";
        getline (cin, input);
        makeLower(input);
        if (input == "attack") {
            damage = player->melee(target);
            damageCalc (goblin, damage, MELEE, ENEMY);
        } else if (input == "quit") {
            cout << "No good! The " << target << " cuts off your escape.\n";
        } else {
            cout << "You stumble, giving the " << target << " an opening!\n";
        }

        if (goblin->getHP() <= 0) {
            break;
        } else {
            cout << endl;
            damage = goblin->melee(target);
            damageCalc (player, damage, MELEE, PLAYER);
        }
    }

    if (player->getHP() <= 0) {
        gameOver();
    } else if (goblin->getHP() <= 0) {
        cout << "\nThe goblin lets out a yell before falling to the ground, dead.\n";
        cout << "\nWith your foe defeated, you look around to regain your bearings.\n";
        cout << "Through the foliage, you spot a cave in the distance.\n";
        cout << "You make your way toward the cave.\n";
        caveScene (player);
    }

    delete goblin;
}

void caveScene (Player* player) {
    bool tryAgain = true;
    string input;

    scenePause();

    cout << "You survey the cave entrance; it is far larger than it first seemed.\n";
    cout << "As you prepare to 'enter' the cave, something catches your eye:\n";
    cout << "A skeleton is next to the entrance, wearing tattered robes.\n";

    if (player->foundSpell() == false) {
        cout << "\nThe skeleton seems to be holding something.\n";
        cout << "Perhaps you should give it a closer 'look'?\n";
    }

    cout << "\nWhat will you do?\n";
    do {
        cout << "> ";
        getline (cin, input);
        makeLower(input);
        if (input == "enter") {
            cout << "You enter the cave." << endl;
            golemBattle (player);
            tryAgain = false;
        } else if (input == "look") {
            cout << "You approach the skeleton. In its hand is a dusty old tome.\n";
            cout << "You gently pull the tome from the skeleton's grasp,\n";
            cout << "then flip through the pages.\n" << endl;

            cout << "The glyphs inside are unlike any you've ever seen,\n";
            cout << "yet somehow, you feel like you understand their meaning.\n" << endl;

            player->getSpell();

            cout << "Before you can read any further, the tome suddenly crumbles into dust!\n";
            cout << "You sigh, but continue on into the cave.\n" << endl;
            golemBattle (player);
            tryAgain = false;
        } else if (input == "quit") {
            saveGame (player, CAVE);
            tryAgain = false;
        } else {
            cout << "That doesn't seem helpful right now." << endl;
            tryAgain = true;
        }
    } while (tryAgain);
}

void golemBattle (Player* player) {
    int damage, turnNum = 1;
    string input, target;

    scenePause ();

    cout << "\nThe path through the cave seems to go on forever, until you round a corner;\n";
    cout << "a large, open cavern fills your view.\n";
    cout << "The cavern is well-lit, illuminated by sunlight shining through the open ceiling.\n";
    cout << "A pile of large rocks is next to the entrance.\n";

    cout << "\nAs you take in the view, you think you see one of the rocks shift.\n";
    cout << "You rub your eyes, staring at the rocks. They remain still.\n";
    cout << "\nYou walk further into the cavern.\n";

    cout << "Suddenly, you hear the rocks trembling behind you.\n";
    cout << "You turn around, eyes widening as the rocks float upwards,\n";
    cout << "forming into a roughly humanoid shape.\n";

    cout << "\nThe golem approaches you.\n";

    cout << "\nYou curse your luck.";
    if (player->foundSword() == true) {
        cout << "\nYou doubt that a sword will be of much help against a golem.\n";
    }

    if (player->foundSpell() == true) {
        cout << "\nYou remember the tome from earlier.\n";
        cout << "Perhaps the 'spell' you learned will be of use?\n";
    }

    Golem* golem = new Golem();
    target = golem->getName();

    while (player->getHP() > 0 && golem->getHP() > 0) {
        player->prompt();
        cout << "> ";
        getline (cin, input);
        makeLower (input);
        if (input == "attack") { // damageCalac (Entity*, damage, damageType)
            damage = player->melee(target);
            damageCalc (golem, damage, MELEE, ENEMY);

            if (player->foundSword()) {
                player->brokeSword();
            }
        } else if (input == "spell") {
            if (player->foundSpell() == false) {
                cout << "What?! You don't know how to use magic!\n" << endl;
            } else {
                damage = player->lightning(target);
                damageCalc (golem, damage, MAGIC, ENEMY);
            }
        } else if (input == "quit") {
            cout << "The " << target << " is blocking the only exit.\n";
        } else {
            cout << "You panic, unsure of what to do against an enemy this large.\n";
        }

        if (golem->getHP() <= 0) {
            break;
        } else {
            cout << endl;
            if (turnNum % 5 == 0) {
                damage = golem->special(target);
                damageCalc(player, damage, MELEE, PLAYER);
            } else if (turnNum % 2 == 0) {
                cout << "The golem takes a moment to reform its body.\n" << endl;
            } else if (turnNum % 2 != 0) {
                damage = golem->melee(target);
                damageCalc(player, damage, MELEE, PLAYER);
            }
        }
        turnNum++;
    }

    if (player->getHP() <= 0) {
        gameOver();
    } else if (golem->getHP() <= 0) {
        cout << "\nThe golem recoils, bits and pieces of rock falling from its body.\n";
        cout << "The golem, struggling to keep itself together, stumbles into the\n";
        cout << "side of the cavern entrance. The collision knocks more pieces away\n";
        cout << "from the golem's body, before it finally crumbles.\n";
        finaleScene(player);
    }

    delete golem;
}
void finaleScene (Player* player) {
    bool tryAgain = true;
    string input;

    scenePause();

    cout << "\nYou let out a sigh of relief, but celebration of your victory\n";
    cout << "is soon cut short as you feel the ground tremble beneath your feet.\n";
    cout << "You look at the cavern entrance just as it collapses.\n";

    cout << "\nYou rush over to the now-collapsed entrance,\n";
    cout << "trying desparately to pull away some of the fallen rocks.\n";
    cout << "Unfortunately, many of the pieces are too large to move.\n";
    cout << "\nYou are trapped.\n";

    do {
        cout << "\n> ";
        getline (cin, input);
        makeLower (input);
        if (input == "quit") {
            saveGame (player, FINALE);
            tryAgain = false;
        } else {
            cout << "You try, but your efforts are futile.\n";
            tryAgain = true;
        }
    } while (tryAgain);
}

void playTest () {
    string testName = "Pat";
    Player* player = new Player (testName);
    Goblin* goblin = new Goblin ();
    Golem* golem = new Golem ();

    goblin->displayStats();
    cout << endl;
    goblin->melee (goblin->getName());
    cout << endl;

    golem->displayStats();
    cout << endl;
    golem->melee (golem->getName());
    cout << endl;
    golem->special (golem->getName());
    cout << endl;

    player->displayStats();
    cout << endl;

    player->foundSword();
    player->foundSpell();
    player->prompt();
    cout << endl;

    delete player;
    delete goblin;
    delete golem;
}

void gameOver () {
    cout << "\nDarkness fills your vision as you feel your life fading away..." << endl;
    cout << "\nGAME OVER\n" << endl;
}