#include <iostream>
#include "Goblin.h"

Goblin::Goblin () : Entity ("Goblin") {
    setHP (30);
    setAtt (15);
    setDef (5);
    setMagAtt (5);
    setMagDef (5);
}

int Goblin::melee (std::string name) {
    std::cout << "The " << name << " lunges forward, swinging its club!" << std::endl;
    return getAtt ();
}
