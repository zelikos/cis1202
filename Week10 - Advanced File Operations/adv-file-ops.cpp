// Patrick Csikos
// CIS 1202 502
// 2020-10-28

#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
using namespace std;

const int NAME_SIZE = 40;

enum Options { DISPLAY, MODIFY };
struct Product {
    long number;
    char name[NAME_SIZE];
    double price;
    int quantity;
};

void clearBuffer();
double getNum();

int showMenu ();
void createFile (fstream&);
void displayFile (fstream&);

void selectRecord (fstream&, Options);
void displayRecord (fstream&, int);
void modifyRecord (fstream&, int);

int main () {
    int menuOpt, choice;
    fstream products ("inventory.dat", ios::in | ios::out | ios::trunc | ios::binary);
    if (!products) {
        cout << "Error creating inventory.dat.\n";
    }

    createFile (products);

    do {
        menuOpt = showMenu();
        switch (menuOpt) {
            case 1:
                displayFile (products);
                break;
            case 2:
                selectRecord(products, DISPLAY);
                break;
            case 3:
                selectRecord(products, MODIFY);
                break;
            case 4:
                cout << "Exiting..." << endl;
                break;
            default:
                cout << "Valid options: 1, 2, 3, 4\n" << endl;
                break;
        }
    } while (menuOpt != 4);

    products.close();
    return 0;
}

void createFile (fstream &products) {
    Product p;

    p.number = 12345;
    strcpy(p.name, "Dog");
    p.price = 11.99;
    p.quantity = 5;
    products.write(reinterpret_cast<char *>(&p), sizeof(p));

    p.number = 12;
    strcpy(p.name, "Cat");
    p.price = 800;
    p.quantity = 4;
    products.write(reinterpret_cast<char *>(&p), sizeof(p));

    p.number = 10101010;
    strcpy(p.name, "Bird");
    p.price = 0.99;
    p.quantity = 500;
    products.write(reinterpret_cast<char *>(&p), sizeof(p));

    p.number = 784723;
    strcpy(p.name, "Fish");
    p.price = 49.95;
    p.quantity = 509;
    products.write(reinterpret_cast<char *>(&p), sizeof(p));

    p.number = 1818;
    strcpy(p.name, "Zebra");
    p.price = 14500;
    p.quantity = 1;
    products.write(reinterpret_cast<char *>(&p), sizeof(p));
}

int showMenu () {
    int choice;

    cout << "Product Inventory\n" << endl;

    cout << "1. Display the entire inventory\n";
    cout << "2. Display a product\n";
    cout << "3. Modify a product\n";
    cout << "4. Exit\n" << endl;

    cout << "> ";

    choice = getNum();

    return choice;
}

void displayFile (fstream &products) {
    products.clear();
    int i = 0;
    while (!products.eof()) {
        displayRecord (products, i);
        i++;
    }
}

void selectRecord (fstream &products, Options option) {
    int choice;
    string selection;

    if (option == DISPLAY) {
        selection = "display";
    } else if (option == MODIFY) {
        selection = "modify";
    }

    products.clear();

    cout << "Record number to " << selection << ": ";
    choice = getNum() - 1;

    if (option == DISPLAY) {
        cout << endl;
        displayRecord (products, choice);
        if (products.eof()) {
            cout << "No record found.\n" << endl;
        }
    } else if (option == MODIFY) {
        modifyRecord (products, choice);
    }
}

void displayRecord (fstream &products, int recNum) {
    Product info;

    products.seekg((sizeof(Product) * recNum), ios::beg);
    products.read(reinterpret_cast<char *>(&info), sizeof(info));

    if (!products.eof()) {
        cout << fixed << setprecision(2);
        // Displays record numbers starting at 1 for readability
        cout << "Record #" << recNum+1 << '\n';
        cout << "Product number: #" << info.number << '\n';
        cout << "Product name: " << info.name << '\n';
        cout << "Price: $" << info.price << '\n';
        cout << "Quantity: " << info.quantity << '\n';
        cout << endl;
    }
}

void modifyRecord (fstream &products, int recNum) {
    Product info;

    products.seekg((sizeof(Product) * recNum), ios::beg);
    products.read(reinterpret_cast<char *>(&info), sizeof(info));

    if (!products.eof()) {
        cout << "Product number: ";
        info.number = getNum();

        cout << "Product name: ";
        cin.getline(info.name, NAME_SIZE);
        // clearBuffer();

        cout << "Price: ";
        info.price = getNum();

        cout << "Quantity: ";
        info.quantity = getNum();

        products.seekp((sizeof(Product) * recNum), ios::beg);
        products.write(reinterpret_cast<char *>(&info), sizeof(info));
    } else {
        cout << "Record not found.\n" << endl;
    }
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}

double getNum () {
    double validNum;
    bool valid = false;

    while(!valid) {
        cin >> validNum;

        if (cin.fail()) {
            cout << "Entry must be a number.\n" << "> ";
        } else {
            valid = true;
        }

        clearBuffer();
    }

    return validNum;
}