// Patrick Csikos
// CIS 1202 502
// 2020-09-11

#include <fstream>
#include <iomanip>
#include <iostream>

int getMenuItem ();
void enterRents (int[], int);
void displayRents (int*, int);
void selectionSort (int[], int);
void displayMemoryLocations (int[], int);

// testing
// void rentDisplay (int[], int);


const int SIZE = 5;

int main () {
    int choice;
    int rentPrices[SIZE] = {0,0,0,0,0};
    int *rentPointer = rentPrices;

    do {
        choice = getMenuItem();
        switch (choice) {
            case 1:
                enterRents (rentPrices, SIZE);
                break;
            case 2:
                //rentDisplay (rentPrices, SIZE);
                displayRents (rentPointer, SIZE);
                break;
            case 3:
                selectionSort (rentPrices, SIZE);
                break;
            case 4:
                displayMemoryLocations (rentPrices, SIZE);
                break;
            case 5:
                std::cout << "Exiting...\n";
                break;
        }
    } while (choice != 5);

    return 0;
}

int getMenuItem () {
    int choice;

    std::cout << "1. Enter rent amounts\n";
    std::cout << "2. Display rent amounts\n";
    std::cout << "3. Sort rent amounts from lowest to highest\n";
    std::cout << "4. Display memory locations\n";
    std::cout << "5. Exit\n" << std::endl;

    do {
        std::cout << "> ";
        std::cin >> choice;
        if (choice > 5 || choice < 1) {
            std::cout << "Invalid selection.\n";
        }
    } while (choice > 5 || choice < 1);
    
    std::cout << std::endl;

    return choice;
}

void enterRents (int prices[], int count) {
    int rentPrice;
    bool isInt;

    for (int i = 0; i < count; i++) {
        isInt = false;
        // ensures that input is a number
        // doubles can be input, but any digit after the decimal is ignored
        do {
            std::cout << "Enter rent amount " << i+1 << " > ";
            if (std::cin >> rentPrice) {
               prices[i] = rentPrice;
               isInt = true;
            } else {
                std::cout << "Amount must be an integer.\n";
                // clears input buffer for next entry
                std::cin.clear();
                std::cin.ignore(__INT_MAX__,'\n');
            }    
        } while (!isInt);
    }

    std::cout << std::endl;
}

void displayRents (int *prices, int count) {
    for (int i = 0; i < count; i++) {
        std::cout << *(prices+i) << std::endl;
    }

    std::cout << std::endl;
}

/*
void rentDisplay (int prices[], int count) {
    for (int i = 0; i < count; i++) {
        std::cout << prices[i] << std::endl;
    }

    std::cout << std::endl;
}
*/

void selectionSort (int prices[], int count) {
    int lowest;
    int tempInt;

    for (int i = 0; i < count; i++) {
        lowest = i;
        for (int j = i + 1; j < count; j++) {
            if (prices[j] < prices[lowest]) {
                lowest = j;
            }
        }
        if (lowest != i) {
            // swaps value at current position with new highest value
            tempInt = prices[i];
            prices[i] = prices[lowest];
            prices[lowest] = tempInt;
        }
    }

    std::cout << "Rent amounts sorted from lowest to highest.\n" << std::endl;
}

void displayMemoryLocations (int prices[], int count) {
    for (int i = 0; i < count; i++) {
        std::cout << &(prices[i]) << std::endl;
    }

    std::cout << std::endl;
}
