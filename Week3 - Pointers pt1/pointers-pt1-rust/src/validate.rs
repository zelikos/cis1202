use std::io;
use std::io::Write;

pub fn get_num () -> f32 {
    let valid_num: f32;

    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Failed to read input.");

        let input: f32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Entry must be a number.");
                continue;
            }
        };

        valid_num = input;
        break;
    }

    valid_num
}

pub fn flush_buffer () {
    std::io::stdout().flush().ok().expect("Failed to flush buffer.");
}