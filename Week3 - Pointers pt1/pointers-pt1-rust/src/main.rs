// Port of pointers_p1.cpp in Rust

mod validate;

fn main () {
    let mut choice;
    let mut rent_prices: Vec<f32> = Vec::new();
    // *rentPointer?;

    loop {
        menu ();
        choice = validate::get_num() as u32;
        match choice {
            1 => enter_rents(&mut rent_prices),
            2 => display_rents(&rent_prices),
            3 => selection_sort(&mut rent_prices),
            4 => display_mem_locations(&rent_prices),
            5 => {
                println!("Exiting...");
                break;
            }
            _ => println!("Invalid selection."),
        }
        println!();
    }
}

fn menu () {
    println!("1. Enter rent amount");
    println!("2. Display rent amounts");
    println!("3. Sort rent amounts from lowest to highest");
    println!("4. Display memory locations");
    println!("5. Exit");
    println!();
    print!("> ");
    validate::flush_buffer();
}

fn enter_rents (prices: &mut Vec<f32>) {
    print!("Enter rent amount: ");
    validate::flush_buffer();
    prices.push(validate::get_num());
}

fn display_rents (prices: &Vec<f32>) {
    if prices.len() == 0 {
        println!("No rents to display.");
    } else {
        for n in 0..prices.len() {
            println!("{:.2}", prices[n]);
        }
    }
}

fn selection_sort (prices: &mut Vec<f32>) {
    let mut lowest;
    let mut temp;

    if prices.len() == 0 {
        println!("No rents to sort.");
    } else {
        for i in 0..prices.len() {
            lowest = i;
            for j in (i+1)..prices.len() {
                if prices[j] < prices[lowest] {
                    lowest = j;
                }
            }
            if lowest != i {
                temp = prices[i];
                prices[i] = prices[lowest];
                prices[lowest] = temp;
            }
        }
        println!("Rents sorted from lowest to highest.");
    }
}

fn display_mem_locations (prices: &Vec<f32>) {
    if prices.len() == 0 {
        println!("None to display.");
    } else {
        for n in 0..prices.len() {
            println!("{:p}", &prices[n]);
        }
    }
}
