// Patrick Csikos
// CIS 1202 502
// 2020-10-07

#include <iomanip>
#include <iostream>

enum Suits { HEARTS, DIAMONDS, SPADES, CLUBS };
enum Ranks { TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };

struct Cards {
    Suits suit;
    Ranks rank;
};

const int SIZE = 52;

void createDeck (Cards[]);
void printDeck (Cards[]);
std::string cardName (Cards);
Cards deal (Cards[]);
std::string winner (Cards, Cards);

int main () {
    Cards deck[SIZE];
    Cards card1, card2;
    std::srand(time(NULL));

    std::cout << "The deck of cards:\n\n";

    createDeck (deck);
    printDeck (deck);
    card1 = deal (deck);
    card2 = deal (deck);
    std::cout << "Card 1 is " << cardName (card1) << '\n';
    std::cout << "Card 2 is " << cardName (card2) << '\n';
    std::cout << winner (card1, card2) << std::endl;
    
    return 0;
}

void createDeck (Cards deck[]) {
    int numCards = 0;
    // int maxCards = 52;

    for (int i = 0; i <= CLUBS; i++) {
        for (int j = 0; j <= ACE; j++) {
            deck[numCards].suit = static_cast<Suits>(i);
            deck[numCards].rank = static_cast<Ranks>(j);
            numCards++;
        }
    }
}

std::string cardName (Cards card) {
    std::string namedCard;

    switch (card.rank) {
        case TWO:
            namedCard = "Two ";
            break;
        case THREE:
            namedCard = "Three ";
            break;
        case FOUR:
            namedCard = "Four ";
            break;
        case FIVE:
            namedCard = "Five ";
            break;
        case SIX:
            namedCard = "Six ";
            break;
        case SEVEN:
            namedCard = "Seven ";
            break;
        case EIGHT:
            namedCard = "Eight ";
            break;
        case NINE:
            namedCard = "Nine ";
            break;
        case TEN:
            namedCard = "Ten ";
            break;
        case JACK:
            namedCard = "Jack ";
            break;
        case QUEEN:
            namedCard = "Queen ";
            break;
        case KING:
            namedCard = "King ";
            break;
        case ACE:
            namedCard = "Ace ";
            break;
    }

    switch (card.suit) {
        case HEARTS:
            namedCard += "of Hearts";
            break;
        case DIAMONDS:
            namedCard += "of Diamonds";
            break;
        case SPADES:
            namedCard += "of Spades";
            break;
        case CLUBS:
            namedCard += "of Clubs";
            break;
    }

    return namedCard;
}

void printDeck (Cards deck[]) {
    for (int i = 0; i < SIZE; i++) {
        std::cout << cardName (deck[i]) << std::endl;
    }
}

Cards deal (Cards deck[]) {
    Cards card;
    int cardNum = (std::rand() % SIZE);

    card.suit = deck[cardNum].suit;
    card.rank = deck[cardNum].rank;

    return card;
}

std::string winner (Cards card1, Cards card2) {
    std::string winningCard;

    if (card1.rank > card2.rank) {
        winningCard = "The winner is the ";
        winningCard += cardName (card1);
    } else if (card2.rank > card1.rank) {
        winningCard = "The winner is the ";
        winningCard += cardName (card2);
    } else {
        winningCard = cardName (card1) + " is tied with " + cardName (card2);
    }

    return winningCard;
}
