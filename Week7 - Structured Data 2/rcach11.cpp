// Patrick Csikos
// CIS 1202 502
// Date

#include <iomanip>
#include <iostream>
using namespace std;

int main () {
    struct Money {
        int dollars;
        int cents;
    };
    
    Money monthlySales[12] = {{10,20},{20,40},{30,60},{40,80},{50,100},{60,120},{70,140},{80,160},{90,180},{100,200},{110,220},{120,240}};
    Money yearlySales;
    
    yearlySales.dollars = 0;
    yearlySales.cents = 0;

    for (int i = 0; i < 12; i++) {
        yearlySales.dollars += monthlySales[i].dollars;
        yearlySales.cents += monthlySales[i].cents;
    
        while (yearlySales.cents > 99) {
            yearlySales.cents -= 100;
            yearlySales.dollars += 1;
        }
    }
    
    cout << yearlySales.dollars << "." << yearlySales.cents << endl;
    return 0;
}

