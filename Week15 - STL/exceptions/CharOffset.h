#ifndef CHAROFFSET_H
#define CHAROFFSET_H

class CharOffset {
    private:
        char start;
        int offset;
    public:
        class invalidCharacterException {};
        class invalidRangeException {};
        class invalidIntException {};

        CharOffset ();
        CharOffset (char, std::string);

        char character ();

        char getStart ();
        int getOffset ();

        void setStart (char);
        void setOffset (std::string);
};

#endif