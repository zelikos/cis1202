#include <ctype.h>
#include <stdexcept>
#include <string>
#include "CharOffset.h"

CharOffset::CharOffset () {
    start = 'a';
    offset = 0;
}

CharOffset::CharOffset (char s, std::string o) {
    setStart(s);
    setOffset(o);
}

char CharOffset::character () {
    char out;

    out = char(int(start) + offset);

    if (!isalpha(out)) {
        throw invalidRangeException();
    } else if (isupper(start) && !isupper(out)) {
        throw invalidRangeException();
    } else if (islower(start) && !islower(out)) {
        throw invalidRangeException();
    }

    return out;
}

char CharOffset::getStart () {
    return start;
}

int CharOffset::getOffset () {
    return offset;
}

void CharOffset::setStart (char s) {
    if (isalpha(s)) {
        start = s;
    } else {
        throw invalidCharacterException();
    }
}

void CharOffset::setOffset (std::string o) {
    int off;

    try {
        off = std::stoi(o);
    } catch (std::invalid_argument){
        throw invalidIntException();
    }

    offset = off;
}