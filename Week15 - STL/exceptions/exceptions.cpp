// Patrick Csikos
// CIS 1202 502
// 2020-12-05

#include <iostream>
#include "CharOffset.h"
using namespace std;

void clearBuffer();

int main () {
    CharOffset* charoff = new CharOffset();

    bool isValid = false;
    char c, offsetChar;
    string input;

    while (!isValid) {
        try {
            cout << "Character: ";
            cin.get(c);
            clearBuffer();
            charoff->setStart(c);
            isValid = true;
        } catch (CharOffset::invalidCharacterException) {
            cout << "ERROR: Character must be alphabetical.\n";
            cout << "Valid characters: A-Z, a-z" << endl;
        }
    }

    isValid = false;
    while (!isValid) {
        try {
            cout << "Offset: ";
            getline(cin, input);
            charoff->setOffset(input);
            isValid = true;
        } catch (CharOffset::invalidIntException) {
            cout << "ERROR: Entry must be an integer." << endl;
        }
    }

    try {
        offsetChar = charoff->character();
        cout << "Offset character: " << offsetChar << endl;
    } catch (CharOffset::invalidRangeException) {
        cout << "ERROR: Out of range." << endl;
    }

    delete charoff;
    return 0;
}

void clearBuffer() {
    cin.clear();
    cin.ignore(__INT_MAX__, '\n');
}
