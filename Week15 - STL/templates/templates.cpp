// Patrick Csikos
// CIS 1202 502
// 2020-12-05

#include <cmath>
#include <iostream>
using namespace std;

template<class T>
T half(const T &num) {
    return (num / 2.0);
}

int half (const int&);

int main () {
    double a = 7.0;
    float b = 5.0f;
    int c = 3;

    cout << half(a) << endl;
    cout << half(b) << endl;
    cout << half(c) << endl;

    return 0;
}

int half (const int &num) {
    int halved;
    double dNum = static_cast<double> (num);

    halved = round((dNum / 2.0));
    return halved;
}
