// Patrick Csikos
// CIS 1202 502
// 2020-08-30

#include <fstream>
#include <iomanip>
#include <iostream>

const int SIZE = 20;

void loadArrays (std::string[SIZE], int[SIZE], float[SIZE], int&);
void calculateValues (int[SIZE], float[SIZE], float[SIZE], int);
void displayTable (std::string[SIZE], int[SIZE], float[SIZE], float[SIZE], int);
void highestValue (std::string[SIZE], float[SIZE], int);
void averageCost (std::string[SIZE], float[SIZE], int);

int main () {
    
    std::string product[SIZE];
    int quantity[SIZE];
    float cost[SIZE];
    float value[SIZE];

    int itemCount = 0;
    
    loadArrays (product, quantity, cost, itemCount);

    calculateValues (quantity, cost, value, itemCount);

    displayTable (product, quantity, cost, value, itemCount);

    highestValue (product, value, itemCount);

    averageCost (product, cost, itemCount);    
    
    return 0;
}

void loadArrays (std::string productID[SIZE], int quantity[SIZE], float cost[SIZE], int& itemCount) {
    // psuedo-code
    std::ifstream input_file;

    input_file.open("01inventory.txt");

    if (input_file) {
        
    } else {
        std::cout << "Unable to open inventory record.";
    }
}